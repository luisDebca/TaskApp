<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%--@elvariable id="task" type="org.debca.demo.Task"--%>
<!DOCTYPE html>
<html>
<head>
    <meta title="Task new">
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <link rel="stylesheet" href="/resources/jquery-ui.css">
    <link rel="stylesheet" href="/resources/jquery.datetimepicker.min.css">
    <script type="text/javascript" src="/resources/jquery.js"></script>
    <script type="text/javascript" src="/resources/jquery-ui.js"></script>
    <script type="text/javascript" src="/resources/jquery.datetimepicker.full.js"></script>
</head>
<body>

<form:form method="post" modelAttribute="task">
    <form:hidden path="id"/>
    <label>Title</label>
    <form:input path="title" maxlength="100" type="text"/><br>
    <label>Content</label>
    <form:textarea path="content" cols="15" rows="5" maxlength="250"/><br>
    <label>Start Date</label>
    <form:input path="start_date" type="text" class="date"/><br>
    <label>End Date</label>
    <form:input path="end_date" type="text" class="date"/><br>

    <button type="submit">Save</button>
</form:form>
<script>
    jQuery(".date").datetimepicker({
        format:'Y-m-d H:i'
    });
</script>
</body>
</html>
