<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
    <meta title="Task">
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>
<body>
    <h1>Task List</h1>
    <%--@elvariable id="task_list" type="java.util.List<org.debca.demo.Task>"--%>
    <c:if test="${empty task_list}"><p style="color: red;font-size: 20px">No registered task</p></c:if>
    <c:if test="${not empty task_list}">
        <table width="100%">
                <thead>
                <tr>
                    <th>Title</th>
                    <th>Content</th>
                    <th>Start Date</th>
                    <th>End Date</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${task_list}" var="item">
                    <tr>
                        <td><a href="/${item.id}/edit">${item.title}</a></td>
                        <td>${item.content}</td>
                        <td>${item.start_date}</td>
                        <td>${item.end_date}</td>
                        <td><a href="/${item.id}/delete">delete</a></td>
                    </tr>
                </c:forEach>
                </tbody>
        </table>
        <br>
        <a href="/delete-all">Delete All</a>
    </c:if>
<br><br>
<a href="/new">Add Task</a>
</body>
</html>