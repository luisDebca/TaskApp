package org.debca.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories("org.debca.demo")
@EntityScan("org.debca.demo")
@SpringBootApplication
public class SimpleDateTimeInputApplication {

    public static void main(String[] args) {
        SpringApplication.run(SimpleDateTimeInputApplication.class, args);
    }
}
