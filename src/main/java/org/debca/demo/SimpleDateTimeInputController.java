package org.debca.demo;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class SimpleDateTimeInputController {

    private TaskRepository taskRepository;

    public SimpleDateTimeInputController(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @GetMapping("/")
    public String showTaskList(ModelMap model){
        model.addAttribute("task_list",taskRepository.findAll());
        return "index";
    }

    @GetMapping("/new")
    public String showNewTaskForm(ModelMap model){
        model.addAttribute("task",new Task());
        return "task_form";
    }

    @PostMapping("/new")
    public String saveNewTask(@ModelAttribute Task task){
        taskRepository.save(task);
        return "redirect:/";
    }

    @GetMapping("/{id}/edit")
    public String showTaskEditForm(@PathVariable Long id, ModelMap model){
        model.addAttribute("task",taskRepository.findById(id).get());
        return "task_form";
    }

    @PostMapping("/*/edit")
    public String updateTask(@ModelAttribute Task task){
        taskRepository.save(task);
        return "redirect:/";
    }

    @GetMapping("/{id}/delete")
    public String deleteTask(@PathVariable Long id){
        taskRepository.deleteById(id);
        return "redirect:/";
    }

    @GetMapping("/delete-all")
    public String deleteAllTask(){
        taskRepository.deleteAll();
        return "redirect:/";
    }
}
